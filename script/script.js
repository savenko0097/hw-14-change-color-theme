'use strict';

const themeButton = document.getElementById('theme-button');
const lightTheme = 'light-theme';  //  bx-toggle-left
const iconTheme = 'bx-toggle-right';     //  bx-toggle-right

const selectedTheme = localStorage.getItem('selected-theme');
const selectedIcon = localStorage.getItem('selected-icon');
  
// body color
function changeTheme(){
    if(document.body.classList.contains(lightTheme)){
        return 'dark';
    }
    else{
        return 'light';
    }
}


function themeIcon() {
    if(themeButton.classList.contains(iconTheme)){
        return 'bx-toggle-left';
    }
    else{
        return 'bx-toggle-right';
    }   
}


// icon left/right

if (selectedTheme === 'dark'){
    document.body.classList.add(lightTheme);
    themeButton.classList.add(iconTheme); //bx-toggle-left
  } else{
    document.body.classList.remove(lightTheme);
    themeButton.classList.remove(iconTheme);

}

// active button
themeButton.addEventListener('click', (event) => {
    // add/remove
    document.body.classList.toggle(lightTheme);
    themeButton.classList.toggle(iconTheme);
  
    
    localStorage.setItem('selected-theme', changeTheme());
    localStorage.setItem('selected-icon', themeIcon());
})